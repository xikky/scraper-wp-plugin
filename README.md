#Media Data Scraper

The MDS ( Media Data Scraper ) is a WordPress plugin used to create an endpoint. This endpoint will be requested by the scraping server to send the scraped data to the WordPress DB. It is required to configure the scraping server to register the endpoint.

To register a new endpoint in the scraping server, add the online site's domain URL to the `WPSITES` constant variable found in Webbot directory, \App\Playcasino\\_helpers\config\config.php

This plugin will create and store all the data received in `$wpdb->prefix . 'media_data_scraper'` table.

The plugin also provides shortcodes to facilitate data fetching efficiently.

##Scrape Shortcode

To use scraped data use the following shortcode:

`[scrape]`

The following parameters can or must be used in order to specify what data is needed. A service below refers to a source of data that is being scraped.

1. `data=` specify what type of data you want to request.
    - `service` - get a service related data. 
    - `first-date` - get the first data scraped for a service.
    - `all-dates` - get a list of data scraped for a service.
2. `service=` must be used to specify the source required for the data type requested. 
      The following is a list of available services:
      - lotto
      - lotto_plus
      - lotto_plus_2
      - power_ball
      - power_ball_plus
      - pick3
      - daily_lotto
      - sportstake8_wf ( weekend fixtures )
      - sportstake8_wr ( weekend result )
      - sportstake8_mwf ( mid week fixtures )
      - sportstake8_mwr ( weekend fixtures )
      - sportstake13_wf ( weekend fixtures )
      - sportstake13_wr ( weekend result )
      - sportstake13_mwf ( mid week fixtures )
      - sportstake13_mwr ( weekend fixtures )
3. `field=` can only be used in conjunction with `data=service` and specifies a specific field
4. `date=` can only be used in conjuction with `data=service` and specifies the date of the data requested. If this is omitted, the current date's data is fetched.

##Testing

For testing import the SQL dump in your WordPress DB found in `assets/testing` directory, to have a sample scraped data.
