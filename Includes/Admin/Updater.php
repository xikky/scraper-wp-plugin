<?php

namespace MDS\Admin;

use MDS\Admin\Libs\AutoUpdater;
use MDS\Admin\Singletons\Config;

class Updater
{

    function update()
    {

        new AutoUpdater(Config::self()->settings('plugin_update_url'), 'plugin', MDS_GIG_PLUGIN_FILE);
    }
}