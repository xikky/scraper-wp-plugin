<?php

namespace MDS\Admin\Libs;

class AutoUpdater
{
    /**
     * The product id (slug) used for this product on the License Manager site.
     * Configured through the class's constructor.
     *
     * @var int     The product id of the related product in the license manager.
     */
    private $product_id;

    /**
     * The API endpoint. Configured through the class's constructor.
     *
     * @var String  The API endpoint.
     */
    private $api_endpoint;

    /**
     * The type of the installation in which this class is being used.
     *
     * @var string  'theme' or 'plugin'.
     */
    private $type;

    /**
     * @var string  The absolute path to the plugin's main file. Only applicable when using the
     *              class with a plugin.
     */
    private $plugin_path;

    /**
     * Initializes the license manager client.
     *
     * @param $api_url      string  The URL to the license manager API (your license server)
     * @param $type         string  The type of project this class is being used in ('theme' or 'plugin')
     * @param $plugin_path  string  The full path to the plugin's main file (only for plugins)
     */
    public function __construct(
        $api_url,
        $type = 'plugin',
        $plugin_path = ''
    ) {

        // Store setup data
        $this->api_endpoint = $api_url;
        $this->type = $type;
        $this->plugin_path = $plugin_path;

        $plugin_path_parts = explode('/', str_replace('\\', '/', $plugin_path));
        $this->product_id = $plugin_path_parts[count($plugin_path_parts) - 2];

        if ($type == 'theme') {
            // Check for updates (for themes)
            add_filter('pre_set_site_transient_update_themes', array($this, 'check_for_update'));
        } elseif ($type == 'plugin') {
            // Check for updates (for plugins)
            add_filter('pre_set_site_transient_update_plugins', array($this, 'check_for_update'));
        }
    }

    /**
     * The filter that checks if there are updates to the theme or plugin
     * using the License Manager API.
     *
     * @param $transient    mixed   The transient used for WordPress theme updates.
     *
     * @return mixed        The transient with our (possible) additions.
     */
    public function check_for_update($transient)
    {

        if (empty($transient->checked)) {
            return $transient;
        }

        if ($this->is_update_available()) {

            $info = $this->get_info();

            if ($this->is_theme()) {
                // Theme update
                $theme_data = wp_get_theme();
                $theme_slug = $theme_data->get_template();

                $transient->response[$theme_slug] = array(
                    'new_version' => $info->new_version,
                    'package'     => $info->package,
                    'url'         => $info->description_url
                );
            } else {

                $transient->response[$info->plugin_path] = (object)array(
                    'new_version' => $info->new_version,
                    'package'     => $info->package,
                    'slug'        => $info->slug
                );
            }
        }

        return $transient;
    }

    //
    // API HELPER FUNCTIONS
    //

    /**
     * Checks the license manager to see if there is an update available for this theme.
     *
     * @return object|bool  If there is an update, returns the license information.
     *                      Otherwise returns false.
     */
    public function is_update_available()
    {

        $info = $this->get_info();

        if ($this->is_api_error($info)) {
            return false;
        }

        if (version_compare($info->new_version, $this->get_local_version(), '>')) {
            return $info;
        }

        return false;
    }

    /**
     * Calls the License Manager API to get the license information for the
     * current product.
     *
     * @return object|bool   The product data, or false if API call fails.
     */
    public function get_info()
    {

        global $wp_version;

        $string_to_encode = $wp_version . "***" .
            $this->get_local_version() . "***" .
            $this->product_id . "***" .
            plugin_basename($this->plugin_path) . "***" .
            get_bloginfo("url");

        $datastring = base64_encode($string_to_encode);

        $post_data = array(
            "datastring" => $datastring
        );

        $info = $this->call_api(
            'updatecheck',
            $post_data
        );

        return $info;
    }

    /**
     * Makes a call to the WP License Manager API.
     *
     * @param $action   String  The API action to invoke on the license manager site
     * @param $params   array   The parameters for the API call
     *
     * @return          array   The API response
     */
    private function call_api($action, $params)
    {

        $url = $this->api_endpoint . $action;

        // Send the request
        $response = wp_remote_post($url, array(
            'headers' => array('Content-Type' => 'application/json; charset=utf-8'),
            'body'    => json_encode($params),
            'method'  => 'POST',
        ));

        if (is_wp_error($response)) {
            return false;
        }

        $response_body = wp_remote_retrieve_body($response);
        $result = json_decode($response_body);

        return $result;
    }

    /**
     * Checks the API response to see if there was an error.
     *
     * @param $response mixed|object    The API response to verify
     *
     * @return bool     True if there was an error. Otherwise false.
     */
    private function is_api_error($response)
    {

        if ($response === false) {
            return true;
        }

        if (!is_object($response)) {
            return true;
        }

        if (isset($response->error)) {
            return true;
        }

        return false;
    }

    /**
     * @return string   The theme / plugin version of the local installation.
     */
    private function get_local_version()
    {

        if ($this->is_theme()) {
            $theme_data = wp_get_theme();

            return $theme_data->Version;
        } else {
            $plugin_data = get_plugin_data($this->plugin_path, false);

            return $plugin_data['Version'];
        }
    }

    private function is_theme()
    {

        return $this->type == 'theme';
    }
}