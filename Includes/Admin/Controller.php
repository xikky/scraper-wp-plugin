<?php

namespace MDS\Admin;

class Controller
{
    public function playcasino($request)
    {
        $request = $request->get_params();

        if (empty($request['key']) || empty($request['value'])) {

            return Router::response("Response has invalid parameters.", false, 502);
        }

        $result = SQL::insert(parse_url(site_url(), PHP_URL_HOST), $request);

        if (!$result) {

            return Router::response("There is a problem with SQL query.", false, 502);
        }

        return Router::response("Data inserted.");
    }

    public function test()
    {
        $data = SQL::get_all();

        return Router::response($data);
    }
}