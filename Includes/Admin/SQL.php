<?php

namespace MDS\Admin;

use MDS\Admin\Singletons\Config;

class SQL
{
    public function __construct()
    {
    }

    public static function create_table()
    {
        global $wpdb;

        $table_name = $wpdb->prefix . 'media_data_scraper';
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name (
                scrape_id mediumint(9) NOT NULL AUTO_INCREMENT,
                scrape_time datetime DEFAULT CURRENT_TIMESTAMP,
                scrape_site varchar(50) NOT NULL,
                scrape_key varchar(50) NOT NULL,
                scrape_value longtext DEFAULT NULL,
                PRIMARY KEY  (scrape_id)
              ) $charset_collate;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);

        $opts = get_option('mds_gig_opts');
        $opts['version'] = MDS_DB_VERSION;
        update_option('mds_gig_opts', $opts);
    }

    public static function insert($site, $data)
    {
        global $wpdb;

        $wpdb->suppress_errors(true);

        $table_name = Config::self()->settings('table');

        foreach ($data as $key => $value) {

            if (!is_string($value)) {

                $data[$key] = serialize($value);

                continue;
            }

            $decoded = json_decode($value);

            if (!empty($decoded)) {

                $data[$key] = serialize((array)$decoded);
            }
        }

        $fields = array(
            'scrape_site'  => $site,
            'scrape_key'   => $data['key'],
            'scrape_value' => $data['value'],
        );

        if ($data['history']) {

            $value = unserialize($data['value']);

            $fields = array_merge($fields, array(
                    'scrape_time' => date('Y-m-d H:i:s', strtotime($value['draw_date']))
                )
            );
        }

        return $wpdb->insert(
            $table_name,
            $fields
        );
    }

    public static function get_data($service, $day = null)
    {
        global $wpdb;

        $table_name = Config::self()->settings('table');
        $site = parse_url(site_url(), PHP_URL_HOST);
        $sql_date_filter = '';

        if (!empty($day)) {

            $sql_date_filter = "DATE(scrape_time) = '$day' AND";
        }

        $result = $wpdb->get_var("SELECT * FROM $table_name WHERE
                          $sql_date_filter
                          scrape_site = '$site' AND 
                          scrape_key = '$service'
                          ORDER BY scrape_time DESC 
                          LIMIT 1", 4);

        return unserialize($result);
    }

    public static function get_all_data($service, $limit = 0)
    {
        global $wpdb;

        $table_name = Config::self()->settings('table');
        $site = parse_url(site_url(), PHP_URL_HOST);
        $sql_limit = '';

        if (!empty($limit)) {
            $sql_limit = "LIMIT $limit";
        }

        $result = $wpdb->get_results("SELECT * FROM $table_name WHERE
                          scrape_time IN (SELECT MAX(scrape_time) FROM $table_name GROUP BY DATE(scrape_time)) AND 
                          scrape_site = '$site' AND  
                          scrape_key = '$service'
                          ORDER BY scrape_time ASC
                          $sql_limit");

        foreach ($result as &$row) {

            $row = unserialize($row->scrape_value);
        }

        return $result;
    }

    public static function get_all()
    {
        global $wpdb;

        $table_name = Config::self()->settings('table');

        return $wpdb->get_results("SELECT * FROM $table_name");
    }
}