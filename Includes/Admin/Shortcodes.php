<?php

namespace MDS\Admin;

use MDS\Admin\Singletons\Cache;

class Shortcodes
{

    public function scrape($atts)
    {

        $atts = shortcode_atts(
            array(
                'data'    => '',
                'service' => '',
                'field'   => '',
                'date'    => '',
            ),
            $atts,
            'scrape'
        );

        $result = '';

        if (!empty($atts['data'])) {

            if (file_exists(MDS_GIG_PLUGIN_DIR . "/assets/admin/data/{$atts['data']}.php")) {

                require(MDS_GIG_PLUGIN_DIR . "/assets/admin/data/{$atts['data']}.php");
            }
        }

        return $result;
    }
}