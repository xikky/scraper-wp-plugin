<?php

namespace MDS\Admin\Abstracts;

/**
 * Class Singleton
 * The singleton pattern is used to restrict the instantiation of a class to a single object.
 */
abstract class Singleton
{

    /**
     * @var array
     */
    protected static $instances = array();

    /**
     * Singleton constructor.
     */
    private function __construct() { }

    /**
     * @return mixed
     */
    public static function self()
    {

        $class = get_called_class();
        if (!array_key_exists($class, self::$instances)) {
            self::$instances[ $class ] = new $class();
        }

        return self::$instances[ $class ];
    }
}