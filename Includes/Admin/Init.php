<?php

namespace MDS\Admin;

class Init
{
    public function __construct()
    {
        $this->define_hooks();
        $this->define_shortcodes();
    }

    private function define_hooks()
    {
        add_action('init', array(new Updater(), 'update'));
        add_action('init', array(new Setup(), 'setup'));

        add_action('rest_api_init', array(new Router(), 'router'));
    }

    private function define_shortcodes()
    {
        add_shortcode('scrape', array(new Shortcodes(), 'scrape'));
    }
}