<?php

namespace MDS\Admin;

use MDS\Admin\Singletons\Config;

class Router
{

    public function router()
    {
        register_rest_route('mds/v1', '/playcasino', array(
            'methods'             => 'POST',
            'callback'            => array(new Controller(), 'playcasino'),
            'args'                => array(
                'key'     => array(
                    'required' => true,
                    'type'     => 'string',
                ),
                'value'   => array(
                    'required' => true,
                    'type'     => 'is_array',
                ),
                'history' => array(
                    'required' => true,
                    'type'     => 'boolean',
                )
            ),
            'permission_callback' => array($this, 'authenticate')
        ));

        register_rest_route('mds/v1', '/test', array(
            'methods'             => 'POST',
            'callback'            => array(new Controller(), 'test'),
            'permission_callback' => array($this, 'authenticate')
        ));
    }

    public function authenticate($request)
    {
        $headers = $request->get_headers();
        var_dump($headers);

        return password_verify($headers['x_signature'][0] . Config::self()->settings('x_id'), $headers['x_hash'][0]);
    }

    public static function response($data = null, $success = true, $status = 200)
    {
        $response = array(
            'success' => $success,
        );

        if (!is_array($data)) {
            $data = (array)$data;
        }

        if ($success) {
            $response['result'] = $data;
        } else {
            $response['error'] = $data;
        }

        $response = new \WP_REST_Response($response);
        $response->set_status($status);
        $response->header('Location', site_url());
        $response->header('Content-Type', 'application/json');

        return $response;
    }
}