<?php

namespace MDS\Admin;

class Setup
{
    public function setup()
    {
        $opts = get_option('mds_gig_opts');

        if (empty($opts)) {

            $opts = array(
                'version' => '0'
            );

            add_option('mds_gig_opts', $opts);
        }

        if (version_compare(MDS_DB_VERSION, $opts['version'], '!=')) {

            SQL::create_table();
        }
    }
}