<?php

namespace MDS\Admin\Singletons;

use MDS\Admin\Abstracts\Singleton;

class Config extends Singleton
{

    public function settings($setting)
    {
        global $wpdb;

        $settings = array(
            'plugin_update_url' => 'https://buildbot.gigmedia.tech/plugins?action=',
            'table'             => $wpdb->prefix . 'media_data_scraper',
            'x_signature'       => 'vEC7LcTNJz6RQgc7KWoR',
            'x_id'              => 'XApbmMXFMQ',
        );

        return $settings[$setting];
    }
}