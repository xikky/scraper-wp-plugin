<?php

/*
Plugin Name: Media Data Scraper
Description: This plugin receives scraped data, stores it and provides shortcodes to fetch the data within the site.
Version: 0.0.8
License: GPL
Text Domain: mds-gig
*/


/**
 * @package modular-parent
 * Class Functions.
 */

define('MDS_GIG_PLUGIN_FILE', __FILE__);
define('MDS_GIG_PLUGIN_DIR', __DIR__);
define('MDS_DB_VERSION', '1.0');

if (!class_exists('\Psr4AutoloaderClass')) {
    require_once MDS_GIG_PLUGIN_DIR . '/autoload.php';
}

$loader = new \Psr4AutoloaderClass;

$loader->addNamespace('MDS\Admin', MDS_GIG_PLUGIN_DIR . '/Includes/Admin');

$loader->register();

new \MDS\Admin\Init();