<?php

if (!empty($atts['service'])) {

    $date = $atts['date'];

    if (!empty($date)) {

        $timestamp = strtotime($date);
        $date = date('Y-m-d', $timestamp);
    }

    $data = \MDS\Admin\SQL::get_data($atts['service'], $date);

    if (!empty($atts['field'])) {

        $data = $data[$atts['field']];
    }

    if (!is_string($data)) {

        $data = json_encode($data);
    }

    $result = $data;
} else {

    $result = "No data available. Shortcode missing -service- parameters.";
}