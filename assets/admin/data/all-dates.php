<?php

if (!empty($atts['service'])) {

    $data = \MDS\Admin\SQL::get_all_data($atts['service']);

    $result = json_encode($data);
} else {

    $result = "No data available. Shortcode missing -service- parameters.";
}